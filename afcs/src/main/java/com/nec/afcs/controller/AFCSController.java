package com.nec.afcs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.nec.afcs.services.AFCSServices;

@RestController
public class AFCSController {

	@Autowired
	private AFCSServices afscServices;
	
	@RequestMapping(method = RequestMethod.GET, value = "/_statusMessage/{_userName}")
	public String returnStatusMessage(@PathVariable String _userName) {
		return afscServices.returnStatusMessage(_userName);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/_status")
	public String applicationStatus() {
		return "Welcome to the AFCS Project.";
	}
	
}
