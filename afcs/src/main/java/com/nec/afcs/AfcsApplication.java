package com.nec.afcs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AfcsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AfcsApplication.class, args);
		System.out.println("The application has started ...");
	}

}
